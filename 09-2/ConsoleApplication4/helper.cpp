#include "helper.h"

using namespace std;

double helper::customRand() {
	random_device rd;
	mt19937 gen(rd());
	uniform_real_distribution<> dis(0, 1);
	return dis(gen);
}

string helper::getCurrentTimeAsString() {
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	strftime(buffer, sizeof(buffer), "_%d-%m-%Y_%I-%M-%S", timeinfo);
	string str(buffer);
	return str;
}