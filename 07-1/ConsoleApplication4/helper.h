#pragma once
#include <random>
#include <chrono>
#include <iostream>

using namespace std;

class helper
{
public:
	helper();
	static double helper::customRand();
	static string getCurrentTimeAsString();
	~helper();
};

