#ifndef CAMERAH
#define CAMERAH

#include "ray.h"
#include "helper.h"

class camera {
public:
	camera();
	camera(float vfov, float aspect);
	camera(vec3 lookfrom, vec3 lookat, vec3 vup, float vfov, float aspect);
	camera(vec3 lookfrom, vec3 lookat, vec3 vup, float vfov, float aspect, float aperture, float focus_dist);
	ray get_ray5(float u, float v);
	ray get_ray12(float u, float v);
	ray get_ray13(float s, float t);
	ray get_ray(float s, float t);
	vec3 origin;
	vec3 lower_left_corner;
	vec3 horizontal;
	vec3 vertical;
	vec3 u, v, w;
	float lens_radius;
};
#endif
