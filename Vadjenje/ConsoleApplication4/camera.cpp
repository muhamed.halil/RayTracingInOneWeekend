#include "camera.h"
vec3 random_in_unit_disk() {
	vec3 p;
	do {
		p = 2.0*vec3(helper::customRand(), helper::customRand(), 0) - vec3(1, 1, 0);
	} while (dot(p, p) >= 1.0);
	return p;
}

camera::camera(){
	lower_left_corner = vec3(-2.0, -1.0, -1.0);
	horizontal = vec3(4.0, 0.0, 0.0);
	vertical = vec3(0.0, 2.0, 0.0);
	origin = vec3(0.0, 0.0, 0.0);
}

camera::camera(float vfov, float aspect){
	float theta = vfov*3.14 / 180;
	float half_height = tan(theta / 2);
	float half_width = aspect * half_height;
	lower_left_corner = vec3(-half_width, -half_height, -1.0);
	horizontal = vec3(0.0, 2*half_height, 0.0);
	vertical = vec3(0.0,0.0,0.0);
}

camera::camera(vec3 lookfrom, vec3 lookat, vec3 vup, float vfov, float aspect) { // vfov is top to bottom in degrees
	vec3 u, v, w;
	float theta = vfov*3.14 / 180;
	float half_height = tan(theta / 2);
	float half_width = aspect * half_height;
	origin = lookfrom;
	w = unit_vector(lookfrom - lookat);
	u = unit_vector(cross(vup, w));
	v = cross(w, u);
	lower_left_corner = vec3(-half_width, -half_height, -1.0);
	lower_left_corner = origin - half_width*u - half_height*v - w;
	horizontal = 2 * half_width*u;
	vertical = 2 * half_height*v;
}

camera::camera(vec3 lookfrom, vec3 lookat, vec3 vup, float vfov, float aspect, float aperture, float focus_dist) { // vfov is top to bottom in degrees
	lens_radius = aperture / 2;
	float theta = vfov*3.14 / 180;
	float half_height = tan(theta / 2);
	float half_width = aspect * half_height;
	origin = lookfrom;
	w = unit_vector(lookfrom - lookat);
	u = unit_vector(cross(vup, w));
	v = cross(w, u);
	lower_left_corner = origin - half_width*focus_dist*u - half_height*focus_dist*v - focus_dist*w;
	horizontal = 2 * half_width*focus_dist*u;
	vertical = 2 * half_height*focus_dist*v;
}

ray camera::get_ray5(float u, float v){
	return ray(origin, lower_left_corner + u*horizontal + v*vertical - origin);
}

ray camera::get_ray12(float u, float v){
	return ray(origin, lower_left_corner + u*horizontal + v*vertical - origin);
}

ray camera::get_ray13(float s, float t){
	return ray(origin, lower_left_corner + s*horizontal + t*vertical - origin);
}


ray camera::get_ray(float s, float t) {
	vec3 rd = lens_radius*random_in_unit_disk();
	vec3 offset = u * rd.x() + v * rd.y();
	return ray(origin + offset, lower_left_corner + s*horizontal + t*vertical - origin - offset);
}


