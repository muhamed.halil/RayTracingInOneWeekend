// ConsoleApplication4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <ctime>
#include <random>
#include <chrono>
#include "float.h"

#include "ray.h"
#include "sphere.h"
#include "hitable_list.h"
#include "camera.h"
#include "material.h"



using namespace std;

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/


/*
* ------------------------------- Vjezbe s casa: crtanje japanske zastave  -------------------------------
*/
void crtajKrugJapanski(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");
	int udaljeenost = 0;
	izlaz << "P3\n" << "512 512" << "\n";
	izlaz << "255" << "\n";
	for (int i = -256; i < 256; i++)
	{
		for (int j = -256; j < 256; j++)
		{
			udaljeenost = sqrt(i*i + j*j);
			if (udaljeenost < 100) {
				izlaz << "255 0 0 ";
			}
			else {
				izlaz << "255 255 255 ";
			}
		}
		izlaz << endl;
	}

	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Vjezbe s casa: crtanje kvadrata kao japanska zastava    -------------------------------
*/
void crtajKvadratJapanski(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");
	int udaljeenost = 0;
	izlaz << "P3\n" << "512 512" << "\n";
	izlaz << "255" << "\n";
	for (int i = -256; i < 256; i++)
	{
		for (int j = -256; j < 256; j++)
		{
			// kvadrat
			if (j>-100 && j<100 && i>-100 && i<100)
			  izlaz << "255 0 0 ";
			else
				izlaz << "255 255 255 ";
		}
		izlaz << endl;
	}

	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Vjezbe s casa: crtaj Kvadrat Japanski S Crnom Ivicom  -------------------------------
*/
void crtajKvadratJapanskiSCrnomIvicom(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");
	int udaljeenost = 0;
	izlaz << "P3\n" << "512 512" << "\n";
	izlaz << "255" << "\n";
	for (int i = -256; i < 256; i++)
	{
		for (int j = -256; j < 256; j++)
		{
			// kvadrat
			if (j>90 && j<100 && i>-100 && i<100)
				izlaz << "0 0 0 ";
			else if (j>-100 && j<100 && i>-100 && i<100)
				izlaz << "255 0 0 ";
			else
				izlaz << "255 255 255 ";
		}
		izlaz << endl;
	}

	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Vjezbe s casa: crtaj kvadrat  -------------------------------
*/
void crtajKvadrat(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	 int nx = 200;
	 int ny = 100;
	 int ns = 10;
	 izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	 for (int j = ny-1; j >= 0; j--)
	 {
	  for (int i = 0; i < nx; i++)
	   {
	    float r = float(i) / float(nx);
	    float g = float(j) / float(ny);
	    float b = 0.2;
	    int ir = int(255.99*r);
	    int ig = int(255.99*g);
	    int ib = int(255.99*b);
	    izlaz << ir << " " << ig << " " << ib << "\n";
	   } /* code */
	 }
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Vjezbe s casa: crtaj kvadrat s klasom  -------------------------------
*/
void crtajKvadratSKlasom(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	for (int j = ny - 1; j >= 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(float(i) / float(nx), float(j) / float(ny), 0.2);
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		} /* code */
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 3: Rays, a simple camera, and background  -------------------------------
*/
vec3 color(const ray& r) {
	vec3 unit_direction = unit_vector(r.direction());
	float t = 0.5*(unit_direction.y() + 1.0);
	return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
}

void rayTracing1(string naziv) {
	ofstream izlaz(naziv+helper::getCurrentTimeAsString()+".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	vec3 lower_left_corner(-2.0, -1.0, -1.0);
	vec3 horizontal(4.0, 0.0, 0.0);
	vec3 vertcial(0.0, 2.0, 0.0);
	vec3 origin(0.0, 0.0, 0.0);
	for (int j = ny-1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			float u = float(i) / float(nx);
			float v = float(j) / float(ny);
			ray r(origin, lower_left_corner + u*horizontal + v*vertcial);
			vec3 col = color(r);
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 4: Adding a sphere  -------------------------------
*/
// Pomocna fnkcija za sferu
bool hit_sphere2(const vec3& center, float radius, const ray &r) {
	vec3 oc = r.origin() - center;
	float a = dot(r.direction(), r.direction());
	float b = 2.0 * dot(oc, r.direction());
	float c = dot(oc, oc) - radius*radius;
	float discriminant = b*b - 4 * a*c;
	return (discriminant > 0);
}

vec3 color2(const ray& r) {
	if (hit_sphere2(vec3(0, 0, -1), 0.5, r))
		return vec3(1, 0, 0);
	vec3 unit_direction = unit_vector(r.direction());
	float t = 0.5*(unit_direction.y() + 1.0);
	return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
}

void rayTracing2(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	vec3 lower_left_corner(-2.0, -1.0, -1.0);
	vec3 horizontal(4.0, 0.0, 0.0);
	vec3 vertcial(0.0, 2.0, 0.0);
	vec3 origin(0.0, 0.0, 0.0);
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			float u = float(i) / float(nx);
			float v = float(j) / float(ny);
			ray r(origin, lower_left_corner + u*horizontal + v*vertcial);
			vec3 col = color2(r);
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}


/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 5: Surface normals and multiple objects.  -------------------------------
*/
// Pomocna fnkcija za sferu
float hit_sphere3(const vec3& center, float radius, const ray &r) {
	vec3 oc = r.origin() - center;
	float a = dot(r.direction(), r.direction());
	float b = 2.0 * dot(oc, r.direction());
	float c = dot(oc, oc) - radius*radius;
	float discriminant = b*b - 4 * a*c;
	if (discriminant < 0)
	{
		return -1.0;
	}
	else {
		return ( -b - sqrt(discriminant) ) / (2.0*a);
	}

}

vec3 color3(const ray& r) {
	float t = hit_sphere3(vec3(0,0,-1), 0.5, r);
	if (t > 0.0) {
		vec3 N = unit_vector(r.point_at_parameter(t) - vec3(0, 0, -1));
		return 0.5 * vec3(N.x()+1, N.y()+1, N.z()+1);
	}
	vec3 unit_direction = unit_vector(r.direction());
	t = 0.5 * (unit_direction.y() + 1.0);
	return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
}

void rayTracing3(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	vec3 lower_left_corner(-2.0, -1.0, -1.0);
	vec3 horizontal(4.0, 0.0, 0.0);
	vec3 vertcial(0.0, 2.0, 0.0);
	vec3 origin(0.0, 0.0, 0.0);
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			float u = float(i) / float(nx);
			float v = float(j) / float(ny);
			ray r(origin, lower_left_corner + u*horizontal + v*vertcial);
			vec3 col = color3(r);
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 5: Surface normals and multiple objects. 2  -------------------------------
*/
vec3 color4(const ray& r, hitable *world) {
	hit_record rec;
	if (world->hit(r, 0.0, FLT_MAX, rec)) {
		return 0.5 * vec3(rec.normal.x() + 1, rec.normal.y() + 1, rec.normal.z() + 1);
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
	}
}

void rayTracing4(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	vec3 lower_left_corner(-2.0, -1.0, -1.0);
	vec3 horizontal(4.0, 0.0, 0.0);
	vec3 vertcial(0.0, 2.0, 0.0);
	vec3 origin(0.0, 0.0, 0.0);
	hitable *list[2];
	list[0] = new sphere(vec3(0, 0, -1), 0.5);
	list[1] = new sphere(vec3(0, -100.5, -1), 100);
	hitable *world = new hitable_list(list, 2);
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			float u = float(i) / float(nx);
			float v = float(j) / float(ny);
			ray r(origin, lower_left_corner + u*horizontal + v*vertcial);

			vec3 p = r.point_at_parameter(2.0);
			vec3 col = color4(r, world);
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 6: Antialiasing  -------------------------------
*/
vec3 color5(const ray& r, hitable *world) {
	hit_record rec;
	if (world->hit(r, 0.0, FLT_MAX, rec)) {
		return 0.5 * vec3(rec.normal.x() + 1, rec.normal.y() + 1, rec.normal.z() + 1);
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
	}
}

void rayTracing5(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	hitable *list[2];
	list[0] = new sphere(vec3(0, 0, -1), 0.5);
	list[1] = new sphere(vec3(0, -100.5, -1), 100);
	hitable *world = new hitable_list(list, 2);
	camera cam;
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++)
			{
				float u = float(i + helper::customRand()) / float(nx);
				float v = float(j + helper::customRand()) / float(ny);

				// !!! !!! !!!
				// ovdje treaba u camera.cpp promjeniti da se koristi metoda get_ray za ova slucaj
				// !!! !!! !!!
				ray r = cam.get_ray5(u, v);

				vec3 p = r.point_at_parameter(2.0);
				col += color5(r, world);

			}
			col /= float(ns);
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 6: Antialiasing -------------------------------
*/
vec3 random_in_unit_sphere6() {
	vec3 p;
	do {
		p = 2.0*vec3(helper::customRand(), helper::customRand(), 0) - vec3(1, 1, 1);
	} while (dot(p, p) >= 1.0);
	return p;
}

vec3 color6(const ray& r, hitable *world) {
	hit_record rec;
	if (world->hit(r, 0.0, FLT_MAX, rec)) {
		vec3 target = rec.p + rec.normal + random_in_unit_sphere6();
		return 0.5 * vec3(rec.normal.x() + 1, rec.normal.y() + 1, rec.normal.z() + 1);
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
	}
}

void rayTracing6(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	hitable *list[2];
	list[0] = new sphere(vec3(0, 0, -1), 0.5);
	list[1] = new sphere(vec3(0, -100.5, -1), 100);
	hitable *world = new hitable_list(list, 2);
	camera cam;
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++)
			{
				float u = float(i + helper::customRand()) / float(nx);
				float v = float(j + helper::customRand()) / float(ny);

				// !!! !!! !!!
				// ovdje treaba u camera.cpp promjeniti da se koristi metoda get_ray za ova slucaj
				// !!! !!! !!!
				ray r = cam.get_ray(u, v);

				vec3 p = r.point_at_parameter(2.0);
				col += color6(r, world);

			}
			col /= float(ns);
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 7: Diffuse materials 2  -------------------------------
*/
void rayTracing7(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	hitable *list[2];
	list[0] = new sphere(vec3(0, 0, -1), 0.5);
	list[1] = new sphere(vec3(0, -100.5, -1), 100);
	hitable *world = new hitable_list(list, 2);
	camera cam;
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++)
			{
				float u = float(i + helper::customRand()) / float(nx);
				float v = float(j + helper::customRand()) / float(ny);

				// !!! !!! !!!
				// ovdje treaba u camera.cpp promjeniti da se koristi metoda get_ray za ova slucaj
				// !!! !!! !!!
				ray r = cam.get_ray5(u, v);

				vec3 p = r.point_at_parameter(2.0);
				col += color6(r, world);
				cout << "j, i, s :" << abs(j- ny) << ", " << i << ", " << s << endl;
				//cout << "Percent complete" << (j*i*s) / (nx*ny*ns) << endl;
			}
			col /= float(ns);
			// Gamma correction
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 8: Metal  -------------------------------
*/
vec3 color8(const ray& r, hitable *world, int depth) {
	hit_record rec;
	if (world->hit(r, 0.0, FLT_MAX, rec)) {
		ray scattered;
		vec3 attenuation;
		if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered))
		{
			return attenuation*color8(scattered, world, depth+1);
		} else {
			return vec3(0,0,0);
		}
		vec3 target = rec.p + rec.normal + random_in_unit_sphere6();
		return 0.5 * vec3(rec.normal.x() + 1, rec.normal.y() + 1, rec.normal.z() + 1);
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
	}
}

void rayTracing8(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	hitable *list[4];
	list[0] = new sphere(vec3(0, 0, -1), 0.5, new lambertian(vec3(0.8, 0.3, 0.3)));
	list[1] = new sphere(vec3(0, -100.5, -1), 100, new lambertian(vec3(0.8, 0.8, 0.0)));
	list[2] = new sphere(vec3(1, 0, -1), 0.5, new metal8(vec3(0.8, 0.6, 0.2)));
	list[3] = new sphere(vec3(-1, 0, -1), 0.5, new metal8(vec3(0.8, 0.8, 0.8)));
	hitable *world = new hitable_list(list, 4);
	camera cam;
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++)
			{
				float u = float(i + helper::customRand()) / float(nx);
				float v = float(j + helper::customRand()) / float(ny);

				// !!! !!! !!!
				// ovdje trebaa u camera.cpp promjeniti da se koristi metoda get_ray za ova slucaj
				// !!! !!! !!!
				ray r = cam.get_ray5(u, v);

				vec3 p = r.point_at_parameter(2.0);
				col += color8(r, world, 0);

			}
			col /= float(ns);
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 8: Metal 2  -------------------------------
*/
vec3 color9(const ray& r, hitable *world, int depth) {
	hit_record rec;
	if (world->hit(r, 0.0, FLT_MAX, rec)) {
		ray scattered;
		vec3 attenuation;
		if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered))
		{
			return attenuation*color9(scattered, world, depth+1);
		} else {
			return vec3(0,0,0);
		}
		vec3 target = rec.p + rec.normal + random_in_unit_sphere6();
		return 0.5 * vec3(rec.normal.x() + 1, rec.normal.y() + 1, rec.normal.z() + 1);
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
	}
}

void rayTracing9(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	hitable *list[4];
	list[0] = new sphere(vec3(0, 0, -1), 0.5, new lambertian(vec3(0.8, 0.3, 0.3)));
	list[1] = new sphere(vec3(0, -100.5, -1), 100, new lambertian(vec3(0.8, 0.8, 0.0)));
	list[2] = new sphere(vec3(1, 0, -1), 0.5, new metal(vec3(0.8, 0.6, 0.2), 0.3));
	list[3] = new sphere(vec3(-1, 0, -1), 0.5, new metal(vec3(0.8, 0.8, 0.8), 1.0));
	hitable *world = new hitable_list(list, 4);
	camera cam;
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++)
			{
				float u = float(i + helper::customRand()) / float(nx);
				float v = float(j + helper::customRand()) / float(ny);

				// !!! !!! !!!
				// ovdje trebaa u camera.cpp promjeniti da se koristi metoda get_ray za ova slucaj
				// !!! !!! !!!
				ray r = cam.get_ray5(u, v);
				vec3 p = r.point_at_parameter(2.0);
				col += color9(r, world, 0);

			}
			col /= float(ns);
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 9: Dielectrics  -------------------------------
*/
vec3 color10(const ray& r, hitable *world, int depth) {
	hit_record rec;
	if (world->hit(r, 0.0, FLT_MAX, rec)) {
		ray scattered;
		vec3 attenuation;
		if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered))
		{
			return attenuation*color10(scattered, world, depth+1);
		} else {
			return vec3(0,0,0);
		}
		vec3 target = rec.p + rec.normal + random_in_unit_sphere6();
		return 0.5 * vec3(rec.normal.x() + 1, rec.normal.y() + 1, rec.normal.z() + 1);
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
	}
}

void rayTracing10(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	hitable *list[4];
	list[0] = new sphere(vec3(0, 0, -1), 0.5, new lambertian(vec3(0.1, 0.2, 0.5)));
	list[1] = new sphere(vec3(0, -100.5, -1), 100, new lambertian(vec3(0.8, 0.8, 0.0)));
	list[2] = new sphere(vec3(1, 0, -1), 0.5, new metal9(vec3(0.8, 0.6, 0.2)));
	list[3] = new sphere(vec3(-1, 0, -1), 0.5, new dielectric(1.5));
	hitable *world = new hitable_list(list, 4);
	camera cam;
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++)
			{
				float u = float(i + helper::customRand()) / float(nx);
				float v = float(j + helper::customRand()) / float(ny);

				// !!! !!! !!!
				// ovdje trebaa u camera.cpp promjeniti da se koristi metoda get_ray za ova slucaj
				// !!! !!! !!!
				ray r = cam.get_ray(u, v);
				vec3 p = r.point_at_parameter(2.0);
				col += color10(r, world, 0);

			}
			col /= float(ns);
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 9: Dielectrics 2  -------------------------------
*/
vec3 color11(const ray& r, hitable *world, int depth) {
	hit_record rec;
	if (world->hit(r, 0.0, FLT_MAX, rec)) {
		ray scattered;
		vec3 attenuation;
		if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered))
		{
			return attenuation*color11(scattered, world, depth+1);
		} else {
			return vec3(0,0,0);
		}
		vec3 target = rec.p + rec.normal + random_in_unit_sphere6();
		return 0.5 * vec3(rec.normal.x() + 1, rec.normal.y() + 1, rec.normal.z() + 1);
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
	}
}

void rayTracing11(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	hitable *list[5];
	list[0] = new sphere(vec3(0, 0, -1), 0.5, new lambertian(vec3(0.1, 0.2, 0.5)));
	list[1] = new sphere(vec3(0, -100.5, -1), 100, new lambertian(vec3(0.8, 0.8, 0.0)));
	list[2] = new sphere(vec3(1, 0, -1), 0.5, new metal9(vec3(0.8, 0.6, 0.2)));
	list[3] = new sphere(vec3(-1, 0, -1), 0.5, new dielectric(1.5));
	list[4] = new sphere(vec3(-1, 0, -1), -0.45, new dielectric(1.5));
	hitable *world = new hitable_list(list, 5);
	camera cam;
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++)
			{
				float u = float(i + helper::customRand()) / float(nx);
				float v = float(j + helper::customRand()) / float(ny);

				// !!! !!! !!!
				// ovdje trebaa u camera.cpp promjeniti da se koristi metoda get_ray za ova slucaj
				// !!! !!! !!!
				ray r = cam.get_ray(u, v);
				vec3 p = r.point_at_parameter(2.0);
				col += color11(r, world, 0);

			}
			col /= float(ns);
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 10: Positionable camera  -------------------------------
*/
vec3 color12(const ray& r, hitable *world, int depth) {
	hit_record rec;
	if (world->hit(r, 0.0, FLT_MAX, rec)) {
		ray scattered;
		vec3 attenuation;
		if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered))
		{
			return attenuation*color12(scattered, world, depth+1);
		} else {
			return vec3(0,0,0);
		}
		vec3 target = rec.p + rec.normal + random_in_unit_sphere6();
		return 0.5 * vec3(rec.normal.x() + 1, rec.normal.y() + 1, rec.normal.z() + 1);
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
	}
}

void rayTracing12(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	hitable *list[2];

	float R = cos(3.14/4);
	list[0] = new sphere(vec3(-R, 0, -1), R, new lambertian(vec3(0, 0, 1)));
	list[1] = new sphere(vec3(R, 0, -1), R, new lambertian(vec3(1, 0, 0)));

	hitable *world = new hitable_list(list, 2);
	camera cam(90, float(nx)/float(ny));
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++)
			{
				float u = float(i + helper::customRand()) / float(nx);
				float v = float(j + helper::customRand()) / float(ny);

				// !!! !!! !!!
				// ovdje trebaa u camera.cpp promjeniti da se koristi metoda get_ray za ova slucaj
				// !!! !!! !!!
				ray r = cam.get_ray(u, v);
				vec3 p = r.point_at_parameter(2.0);
				col += color12(r, world, 0);

			}
			col /= float(ns);
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 10: Positionable camera 2  -------------------------------
*/
vec3 color13(const ray& r, hitable *world, int depth) {
	hit_record rec;
	if (world->hit(r, 0.0, FLT_MAX, rec)) {
		ray scattered;
		vec3 attenuation;
		if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered))
		{
			return attenuation*color13(scattered, world, depth+1);
		} else {
			return vec3(0,0,0);
		}
		vec3 target = rec.p + rec.normal + random_in_unit_sphere6();
		return 0.5 * vec3(rec.normal.x() + 1, rec.normal.y() + 1, rec.normal.z() + 1);
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
	}
}

void rayTracing13(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	hitable *list[2];

	float R = cos(3.14/4);
	list[0] = new sphere(vec3(-R, 0, -1), R, new lambertian(vec3(0, 0, 1)));
	list[1] = new sphere(vec3(R, 0, -1), R, new lambertian(vec3(1, 0, 0)));

	hitable *world = new hitable_list(list, 2);
	camera cam(vec3(-2,2,1), vec3(0,0,-1), vec3(0,1,0), 90, float(nx)/float(ny));
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++)
			{
				float u = float(i + helper::customRand()) / float(nx);
				float v = float(j + helper::customRand()) / float(ny);

				// !!! !!! !!!
				// ovdje trebaa u camera.cpp promjeniti da se koristi metoda get_ray za ova slucaj
				// !!! !!! !!!
				ray r = cam.get_ray(u, v);
				vec3 p = r.point_at_parameter(2.0);
				col += color13(r, world, 0);

			}
			col /= float(ns);
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 11: Defocus Blur  -------------------------------
*/
vec3 color14(const ray& r, hitable *world, int depth) {
	hit_record rec;
	if (world->hit(r, 0.0, FLT_MAX, rec)) {
		ray scattered;
		vec3 attenuation;
		if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered))
		{
			return attenuation*color14(scattered, world, depth+1);
		} else {
			return vec3(0,0,0);
		}
		vec3 target = rec.p + rec.normal + random_in_unit_sphere6();
		return 0.5 * vec3(rec.normal.x() + 1, rec.normal.y() + 1, rec.normal.z() + 1);
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
	}
}

void rayTracing14(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	hitable *list[2];

	float R = cos(3.14/4);
	list[0] = new sphere(vec3(-R, 0, -1), R, new lambertian(vec3(0, 0, 1)));
	list[1] = new sphere(vec3(R, 0, -1), R, new lambertian(vec3(1, 0, 0)));

	vec3 lookfrom(3, 3, 2);
	vec3 lookat(0, 0, -1);
	float dist_to_focus = (lookfrom-lookat).length();
	float aperture = 2.0;

	hitable *world = new hitable_list(list, 2);
	camera cam(lookfrom, lookat, vec3(0,1,0), 20, float(nx)/float(ny), aperture, dist_to_focus);
	for (int j = ny - 1; j > 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++)
			{
				float u = float(i + helper::customRand()) / float(nx);
				float v = float(j + helper::customRand()) / float(ny);

				// !!! !!! !!!
				// ovdje trebaa u camera.cpp promjeniti da se koristi metoda get_ray za ova slucaj
				// !!! !!! !!!
				ray r = cam.get_ray(u, v);
				vec3 p = r.point_at_parameter(2.0);
				col += color14(r, world, 0);

			}
			col /= float(ns);
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter 12: Where next?  -------------------------------
*/
vec3 colorFinal(const ray& r, hitable* world, int depth) {
	hit_record rec;
	if (world->hit(r, 0.001, FLT_MAX, rec)) {
		ray scattered;
		vec3 attenuation;
		if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered)) {
			return attenuation*colorFinal(scattered, world, depth + 1);
		}
		else {
			return vec3(0, 0, 0);
		}
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5*(unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t*vec3(0.5, 0.7, 1.0);
	}
}


hitable *random_scene() {
	int n = 500;
	hitable **list = new hitable*[n + 1];
	list[0] = new sphere(vec3(0, -1000, 0), 1000, new lambertian(vec3(0.5, 0.5, 0.5)));
	int i = 1;
	for (int a = -11; a < 11; a++) {
		for (int b = -11; b < 11; b++) {
			float choose_mat = helper::customRand();
			vec3 center(a + 0.9*helper::customRand(), 0.2, b + 0.9*helper::customRand());
			if ((center - vec3(4, 0.2, 0)).length() > 0.9) {
				if (choose_mat < 0.8) {  // diffuse
					list[i++] = new sphere(center, 0.2, new lambertian(vec3(helper::customRand()*helper::customRand(), helper::customRand()*helper::customRand(), helper::customRand()*helper::customRand())));
				}
				else if (choose_mat < 0.95) { // metal
					list[i++] = new sphere(center, 0.2,
						new metal(vec3(0.5*(1 + helper::customRand()), 0.5*(1 + helper::customRand()), 0.5*(1 + helper::customRand())), 0.5*helper::customRand()));
				}
				else {  // glass
					list[i++] = new sphere(center, 0.2, new dielectric(1.5));
				}
			}
		}
	}

	list[i++] = new sphere(vec3(0, 1, 0), 1.0, new dielectric(1.5));
	list[i++] = new sphere(vec3(-4, 1, 0), 1.0, new lambertian(vec3(0.4, 0.2, 0.1)));
	list[i++] = new sphere(vec3(4, 1, 0), 1.0, new metal(vec3(0.7, 0.6, 0.5), 0.0));

	return new hitable_list(list, i);
}

void rayTracingFinal(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");
	int nx = 1200;
	int ny = 800;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	hitable *list[5];
	float R = cos(3.14 / 4);
	list[0] = new sphere(vec3(0, 0, -1), 0.5, new lambertian(vec3(0.1, 0.2, 0.5)));
	list[1] = new sphere(vec3(0, -100.5, -1), 100, new lambertian(vec3(0.8, 0.8, 0.0)));
	list[2] = new sphere(vec3(1, 0, -1), 0.5, new metal(vec3(0.8, 0.6, 0.2), 0.0));
	list[3] = new sphere(vec3(-1, 0, -1), 0.5, new dielectric(1.5));
	list[4] = new sphere(vec3(-1, 0, -1), -0.45, new dielectric(1.5));
	hitable *world = new hitable_list(list, 5);
	world = random_scene();

	vec3 lookfrom(13, 2, 3);
	vec3 lookat(0, 0, 0);
	float dist_to_focus = 10.0;
	float aperture = 0.1;

	camera cam(lookfrom, lookat, vec3(0, 1, 0), 20, float(nx) / float(ny), aperture, dist_to_focus);

	for (int j = ny - 1; j >= 0; j--) {
		for (int i = 0; i < nx; i++) {
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++) {
				float u = float(i + helper::customRand()) / float(nx);
				float v = float(j + helper::customRand()) / float(ny);
				ray r = cam.get_ray(u, v);
				vec3 p = r.point_at_parameter(2.0);
				col += colorFinal(r, world, 0);
				cout << "j: " << j << ", i: " << i << ", s: " << s << endl;
			}
			col /= float(ns);
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		}
	}

	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Chapter :  -------------------------------
*/
int main()
{
	//crtajKrugJapanski("KrugJapanski");
	//crtajKvadratJapanski("KvadratJapanski");
	//crtajKvadratJapanskiSCrnomIvicom("KvadratJapanskiIvicaCrna");
	//crtajKvadrat("Kvadrat");
	crtajKvadratSKlasom("KvadratSKlasom");

	// Knjiga:

	//rayTracing1("rayTracing1");
	//rayTracing2("rayTracing2");
	//rayTracing3("rayTracing3");
	//rayTracing4("rayTracing4");
	//rayTracing5("rayTracing5");
	//rayTracing6("rayTracing6"); // ima problem - treba vidit sta je
	//rayTracing7("rayTracing7"); // ime problem - treba vidit sta je
	rayTracing8("rayTracing8");
	//rayTracing9("rayTracing9");
	//rayTracing10("rayTracing10"); // ime problem - treba vidit sta je
	//rayTracing11("rayTracing11"); // ima problem - treba vidit sta je
	//rayTracing12("rayTracing12");
	//rayTracing13("rayTracing13");
	//rayTracing14("rayTracing14");
	//rayTracingFinal("rayTracingFinal");
	return 0;
}