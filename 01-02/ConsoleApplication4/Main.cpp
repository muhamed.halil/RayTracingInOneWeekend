// ConsoleApplication4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include "helper.h"
#include "vec3.h"

using namespace std;

void crtajKrugJapanski(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");
	int udaljeenost = 0;
	izlaz << "P3\n" << "512 512" << "\n";
	izlaz << "255" << "\n";
	for (int i = -256; i < 256; i++)
	{
		for (int j = -256; j < 256; j++)
		{
			udaljeenost = sqrt(i*i + j*j);
			if (udaljeenost < 100) {
				izlaz << "255 0 0 ";
			}
			else {
				izlaz << "255 255 255 ";
			}
		}
		izlaz << endl;
	}

	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Vjezbe s casa: crtanje kvadrata kao japanska zastava    -------------------------------
*/
void crtajKvadratJapanski(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");
	int udaljeenost = 0;
	izlaz << "P3\n" << "512 512" << "\n";
	izlaz << "255" << "\n";
	for (int i = -256; i < 256; i++)
	{
		for (int j = -256; j < 256; j++)
		{
			// kvadrat
			if (j>-100 && j<100 && i>-100 && i<100)
			  izlaz << "255 0 0 ";
			else
				izlaz << "255 255 255 ";
		}
		izlaz << endl;
	}

	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Vjezbe s casa: crtaj Kvadrat Japanski S Crnom Ivicom  -------------------------------
*/
void crtajKvadratJapanskiSCrnomIvicom(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");
	int udaljeenost = 0;
	izlaz << "P3\n" << "512 512" << "\n";
	izlaz << "255" << "\n";
	for (int i = -256; i < 256; i++)
	{
		for (int j = -256; j < 256; j++)
		{
			// kvadrat
			if (j>90 && j<100 && i>-100 && i<100)
				izlaz << "0 0 0 ";
			else if (j>-100 && j<100 && i>-100 && i<100)
				izlaz << "255 0 0 ";
			else
				izlaz << "255 255 255 ";
		}
		izlaz << endl;
	}

	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Vjezbe s casa: crtaj kvadrat  -------------------------------
*/
void crtajKvadrat(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	 int nx = 200;
	 int ny = 100;
	 int ns = 10;
	 izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	 for (int j = ny-1; j >= 0; j--)
	 {
	  for (int i = 0; i < nx; i++)
	   {
	    float r = float(i) / float(nx);
	    float g = float(j) / float(ny);
	    float b = 0.2;
	    int ir = int(255.99*r);
	    int ig = int(255.99*g);
	    int ib = int(255.99*b);
	    izlaz << ir << " " << ig << " " << ib << "\n";
	   } /* code */
	 }
	izlaz.close();
}

/*
* ------------------------------- Kraj logicke cijeline -------------------------------
*/

/*
* ------------------------------- Vjezbe s casa: crtaj kvadrat s klasom  -------------------------------
*/
void crtajKvadratSKlasom(string naziv) {
	ofstream izlaz(naziv + helper::getCurrentTimeAsString() + ".ppm");

	int nx = 200;
	int ny = 100;
	int ns = 10;
	izlaz << "P3\n" << nx << " " << ny << "\n255\n";
	for (int j = ny - 1; j >= 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col(float(i) / float(nx), float(j) / float(ny), 0.2);
			int ir = int(255.99*col[0]);
			int ig = int(255.99*col[1]);
			int ib = int(255.99*col[2]);
			izlaz << ir << " " << ig << " " << ib << "\n";
		} /* code */
	}
	izlaz.close();
}

/*
* ------------------------------- Chapter :  -------------------------------
*/
int main()
{
	//crtajKrugJapanski("KrugJapanski");
	//crtajKvadratJapanski("KvadratJapanski");
	//crtajKvadratJapanskiSCrnomIvicom("KvadratJapanskiIvicaCrna");
	//crtajKvadrat("Kvadrat");
	crtajKvadratSKlasom("KvadratSKlasom");
	return 0;
}