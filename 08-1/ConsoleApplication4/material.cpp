#include "material.h"

vec3 reflect(const vec3& v, const vec3& n) {
	return v - 2 * dot(v, n)*n;
}

vec3 random_in_unit_sphere() {
	vec3 p;
	do {
		p = 2.0*vec3(helper::customRand(), helper::customRand(), helper::customRand()) - vec3(1, 1, 1);
	} while (p.squared_length() >= 1.0);
	return p;
}
